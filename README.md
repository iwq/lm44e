## Introduction
This repository is for the collection of work relating to the LM44E project, AKA the "Libre Mother 4 For Everyone" project: working title for a game intending, in a similar vein to [Oddity](https://wikiless.org/wiki/Oddity_(video_game)), as a game attempting to capture the same emotions that the Mother series did. However, I am not going to hide development in the same way that Oddity has: the whole everything that goes into this game: all of the images made, all the text written, all the ideas had, all the tracks produced: EVERYTHING made will be completely dedicated to the Public Domain.

## Who are you?
I am iwq. I live in the United States and I work a small local restaurant job, enough to get by. I hold digital privacy and human rights in a very high regard.

## Anything I can help with?
- If you fork this, I ask (but don't require) that you also license your work under something either copyleft, permissive, or dedicate it to the Public Domain as I have. I will only upstream changes that I can make public domain, so keep that in mind if you are going to make a pull request.
- I will eventually set up some sort of monero wallet or something for donations. Maybe. Probably once this starts showing some real promise.

## But why?
I wanted a project to work on in the free time, and I am a big fan of libre software. The Mother series is one of my favorite game series: I decided to make my own project to try and capture the feelings and such from those gems. Nothing much to it. Hope something great comes from this. :)

## Disclaimer
To be crystal clear, this project has absolutely no relation to the Mother series, Shigesato Itoi, HAL labs, Nintendo, or any affiliated parties. Furthermore, this project has no relation to the fan work _[Mother 4](https://mother4.org)_ or the developers behind _Mother 4_; and this project is in no way related to _Oddity_ or the developers behind _Oddity_.